//
//  TickSliderBackgroundView.swift
//  CustomSliderExample
//
//  Created by Zakk Hoyt on 6/9/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import UIKit


enum TickSliderOrientation: Int {
    case horizontal
    case vertical
}


class TickSliderBackgroundView: UIView {
    
    
    var orientation: TickSliderOrientation = .horizontal {
        didSet {
            update()
        }
    }
    
    var value: CGFloat = 0.5 {
        didSet {
            update()
        }
    }
    
    var minimumValue: CGFloat = 0.0 {
        didSet {
            update()
        }
    }
    
    var maximiumValue: CGFloat = 1.0 {
        didSet {
            update()
        }
    }

    var trackWidth: CGFloat = 20.0 {
        didSet {
            update()
        }
    }

    var minimumTrackTintColor: UIColor = .white {
        didSet {
            update()
        }
    }
    
    var maximumTrackTintColor: UIColor = .clear {
        didSet {
            update()
        }
    }
    
    var borderWidth: CGFloat = 0.5 {
        didSet {
            update()
        }
    }
    
    var divisions: Int = 10 {
        didSet {
            update()
        }
    }
    
    var snapsToDivisions: Bool = false {
        didSet {
            update()
        }
    }
    
    var minimumTrackDivisionColor: UIColor = .darkGray {
        didSet {
            update()
        }
    }
    
    var maximumTrackDivisionColor: UIColor = .white {
        didSet {
            update()
        }
    }

    var scaleWhenTouching: Bool = true {
        didSet {
            update()
        }
    }
    
    var scaleDuration: TimeInterval = 0.15 {
        didSet {
            update()
        }
    }
    
    private var borderColor: UIColor {
        get {
            if minimumTrackTintColor != .clear {
                return minimumTrackTintColor
            } else if maximumTrackTintColor != .clear {
                return maximumTrackTintColor
            } else {
                return tintColor
            }
        }
    }

    

    
    private var minimumLayer: CAShapeLayer?
    private var maximumLayer: CAShapeLayer?
    private var divisionLayers: [CAShapeLayer] = []
    
    
    func update() {
        

        layer.masksToBounds = true
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        
        minimumLayer?.removeFromSuperlayer()
        minimumLayer = nil
        minimumLayer = CAShapeLayer()
        layer.addSublayer(minimumLayer!)
        
        
        maximumLayer?.removeFromSuperlayer()
        maximumLayer = nil
        maximumLayer = CAShapeLayer()
        layer.addSublayer(maximumLayer!)
        
        for divisionLayer in divisionLayers {
            divisionLayer.removeFromSuperlayer()
        }
        divisionLayers.removeAll()

        if orientation == .horizontal {
            
            layer.cornerRadius = bounds.height / 4.0
            
            let normalizedValue = (value - minimumValue) / (maximiumValue - minimumValue)
            let valueThreshold = bounds.size.width * normalizedValue
            let divisionWidth = bounds.width / CGFloat(divisions)
            
            for i in 0..<(divisions - 1) {
                let divisionLayer = CAShapeLayer()
                divisionLayers.append(divisionLayer)
                layer.addSublayer(divisionLayer)
                
                let x: CGFloat = divisionWidth + CGFloat(i) * divisionWidth
                let y: CGFloat = 0
                let width: CGFloat = 0.5
                let height: CGFloat = bounds.height
                let rect = CGRect(x: x, y: y, width: width, height: height)
                divisionLayer.path = UIBezierPath(rect: rect).cgPath
                if x > valueThreshold {
                    divisionLayer.color = maximumTrackDivisionColor
                } else {
                    divisionLayer.color = minimumTrackDivisionColor
                }
            }
            
            do {
                var x: CGFloat = 0
                x = max(x, 0)
                let y: CGFloat = 0
                var width: CGFloat = bounds.size.width * normalizedValue
                width = min(bounds.width, width)
                let height = bounds.height
                let rect = CGRect(x: x, y: y, width: width, height: height)
                minimumLayer?.path = UIBezierPath(rect: rect).cgPath
                minimumLayer?.color = minimumTrackTintColor
            }
            
            do {
                let x: CGFloat = bounds.size.width * normalizedValue
                let y: CGFloat = 0
                var width: CGFloat = bounds.size.width - x
                width = max(width, 0)
                let height: CGFloat = bounds.height
                let rect = CGRect(x: x, y: y, width: width, height: height)
                maximumLayer?.path = UIBezierPath(rect: rect).cgPath
                maximumLayer?.color = maximumTrackTintColor
            }
        } else {

            layer.cornerRadius = bounds.width / 4.0

            let normalizedValue = (value - minimumValue) / (maximiumValue - minimumValue)
            let valueThreshold = bounds.size.height - (bounds.size.height * normalizedValue)
            let divisionHeight = bounds.height / CGFloat(divisions)
            
            for i in 0..<(divisions - 1) {
                let divisionLayer = CAShapeLayer()
                divisionLayers.append(divisionLayer)
                layer.addSublayer(divisionLayer)
                
                let x: CGFloat = 0
                let y: CGFloat = divisionHeight + CGFloat(i) * divisionHeight
                let width: CGFloat = bounds.width
                let height: CGFloat = 0.5
                let rect = CGRect(x: x, y: y, width: width, height: height)
                divisionLayer.path = UIBezierPath(rect: rect).cgPath
                if y < valueThreshold {
                    divisionLayer.color = maximumTrackDivisionColor
                } else {
                    divisionLayer.color = minimumTrackDivisionColor
                }
            }
            
            do {
                let x: CGFloat = 0
                var y: CGFloat = bounds.size.height - (bounds.size.height * normalizedValue)
                y = max(y, 0)
                let width: CGFloat = bounds.width
                var height = bounds.size.height - y
                height = min(bounds.height, height)
                let rect = CGRect(x: x, y: y, width: width, height: height)
                minimumLayer?.path = UIBezierPath(rect: rect).cgPath
                minimumLayer?.color = minimumTrackTintColor
            }
            
            do {
                let x: CGFloat = 0
                let y: CGFloat = 0
                let width: CGFloat = bounds.width
                var height = bounds.size.height - (bounds.size.height * normalizedValue)
                height = max(height, 0)
                height = min(height, bounds.height)
                let rect = CGRect(x: x, y: y, width: width, height: height)
                maximumLayer?.path = UIBezierPath(rect: rect).cgPath
                maximumLayer?.color = maximumTrackTintColor
            }
        }
    }
}


extension UIColor {
    var opacity: Float {
        var alpha: CGFloat = 1.0
        self.getRed(nil, green: nil, blue: nil, alpha: &alpha)
        return Float(alpha)
    }
}

extension CAShapeLayer {
    var color: UIColor? {
        set {
            guard let color = newValue else {
                self.fillColor = nil
                self.opacity = 0.0
                return
            }
            self.opacity = color.opacity
            self.fillColor = color.cgColor
        }
        get {
            guard let cgColor = self.fillColor else {
                return nil
            }
            return UIColor(cgColor: cgColor)
        }
    }
}
