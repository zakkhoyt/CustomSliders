//
//  TickSlider.swift
//  CustomSliderExample
//
//  Created by Zakk Hoyt on 6/9/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import UIKit





@IBDesignable
class TickSlider: UIControl {
    
    @IBInspectable
    var minimumTrackTintColor: UIColor {
        get {
            return tickSliderBackgroundView.minimumTrackTintColor
        }
        set {
            tickSliderBackgroundView.minimumTrackTintColor = newValue
        }
    }
    
    @IBInspectable
    var maximumTrackTintColor: UIColor {
        get {
            return tickSliderBackgroundView.maximumTrackTintColor
        }
        set {
            tickSliderBackgroundView.maximumTrackTintColor = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return tickSliderBackgroundView.borderWidth
        }
        set {
            tickSliderBackgroundView.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var minimumValue: CGFloat {
        get {
            return tickSliderBackgroundView.minimumValue
        }
        set {
            tickSliderBackgroundView.minimumValue = newValue
        }
    }
    
    @IBInspectable
    var maximiumValue: CGFloat {
        get {
            return tickSliderBackgroundView.maximiumValue
        }
        set {
            tickSliderBackgroundView.maximiumValue = newValue
        }
    }
    
    @IBInspectable
    var value: CGFloat {
        get {
            return tickSliderBackgroundView.value
        }
        set {
            tickSliderBackgroundView.value = newValue
            updateKnob()
        }
    }
    
    
    @IBInspectable
    var scaleWhenTouching: Bool {
        get {
            return tickSliderBackgroundView.scaleWhenTouching
        }
        set {
            tickSliderBackgroundView.scaleWhenTouching = newValue
        }
    }
    
    @IBInspectable
    var scaleDuration: TimeInterval {
        get {
            return tickSliderBackgroundView.scaleDuration
        }
        set {
            tickSliderBackgroundView.scaleDuration = newValue
        }
    }
    
    @IBInspectable
    var divisions: Int {
        get {
            return tickSliderBackgroundView.divisions
        }
        set {
            tickSliderBackgroundView.divisions = newValue
        }
    }
    
    @IBInspectable
    var snapsToDivisions: Bool {
        get {
            return tickSliderBackgroundView.snapsToDivisions
        }
        set {
            tickSliderBackgroundView.snapsToDivisions = newValue
        }
    }
    
    @IBInspectable
    var minimumTrackDivisionColor: UIColor {
        get {
            return tickSliderBackgroundView.minimumTrackDivisionColor
        }
        set {
            tickSliderBackgroundView.minimumTrackDivisionColor = newValue
        }
    }
    
    @IBInspectable
    var maximumTrackDivisionColor: UIColor {
        get {
            return tickSliderBackgroundView.maximumTrackDivisionColor
        }
        set {
            tickSliderBackgroundView.maximumTrackDivisionColor = newValue
        }
    }

    
    @IBInspectable
    var trackWidth: CGFloat {
        get {
            return tickSliderBackgroundView.trackWidth
        }
        set {
            tickSliderBackgroundView.trackWidth = newValue
            setNeedsLayout()
        }
    }
    
    
    
    @IBInspectable
    var showKnob: Bool = true {
        didSet {
            updateKnob()
        }
    }
    
    
    
    @IBInspectable
    var knobImage: UIImage? {
        get {
            return knobView.image
        }
        set {
            knobView.image = newValue
        }
    }

    
    private var orientation: TickSliderOrientation {
        get {
            return tickSliderBackgroundView.orientation
        }
        set {
            tickSliderBackgroundView.orientation = newValue
        }
    }
    
    
    private var knobView = TickKnobView()
    private var tickSliderBackgroundView = TickSliderBackgroundView()
    private var ignoreTouches = false
    private var touchPointBegan: CGPoint = .zero
    private var touchValueBegan: CGFloat = 0
    private var valueChangeDate = Date()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if tickSliderBackgroundView.superview == nil {
            self.backgroundColor = .clear
            addSubview(tickSliderBackgroundView)
        }
        
        if bounds.width > bounds.height {
            tickSliderBackgroundView.orientation = .horizontal
            let frame = CGRect(x: 0, y: 0, width: bounds.width, height: trackWidth)
            tickSliderBackgroundView.frame = frame
            tickSliderBackgroundView.center = CGPoint(x: bounds.midX, y: bounds.midY)
        } else {
            tickSliderBackgroundView.orientation = .vertical
            let frame = CGRect(x: 0, y: 0, width: trackWidth, height: bounds.height)
            tickSliderBackgroundView.frame = frame
            tickSliderBackgroundView.center = CGPoint(x: bounds.midX, y: bounds.midY)
        }
        
        tickSliderBackgroundView.update()
        updateKnob()
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("Begin")
        guard let touch = touches.first else {
            return
        }
        
        let point = touch.location(in: self)
        
        self.touchPointBegan = point
        self.touchValueBegan = self.value
        
        self.sendActions(for: .touchDown)
        self.ignoreTouches = false
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("Moved")
        processTouches(touches, with: event)
        
        // Throttle the valueChanged event
        let elapsed = Date().timeIntervalSince(valueChangeDate)
        if elapsed > 0.25 {
            valueChangeDate = Date()
            sendActions(for: .valueChanged)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("Ended")
        processTouches(touches, with: event)
        
        sendActions(for: .valueChanged)
        sendActions(for: .touchUpInside)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("Cancelled")
        sendActions(for: .touchCancel)
        
    }
    
    private func processTouches(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        let point = touch.location(in: self)
        
        if orientation == .horizontal {
            let deltaX = point.x - touchPointBegan.x
            let percentChanged = deltaX / bounds.width * transform.a
            let maxValueDelta = maximiumValue - minimumValue
            var newValue = touchValueBegan + maxValueDelta * percentChanged
            
            if snapsToDivisions {
                var closestIndex: Int = 0
                var closestDiff: CGFloat = CGFloat.greatestFiniteMagnitude
                
                let valuePerDivision = (maximiumValue - minimumValue) / CGFloat(divisions)
                for i in 0...divisions {
                    let divisionValue = minimumValue + CGFloat(i) * valuePerDivision
                    let diff = fabs(newValue - divisionValue)
                    if diff < closestDiff {
                        closestDiff = diff
                        closestIndex = i
                    }
                }
                
                newValue = minimumValue + CGFloat(closestIndex) * valuePerDivision
            } else {
                newValue = min(newValue, maximiumValue)
                newValue = max(newValue, minimumValue)
            }
            value = newValue
        } else {
            let deltaY = touchPointBegan.y - point.y
            let percentChanged = deltaY / bounds.height * transform.d
            let maxValueDelta = maximiumValue - minimumValue
            var newValue = touchValueBegan + maxValueDelta * percentChanged
            
            
            if snapsToDivisions {
                var closestIndex: Int = 0
                var closestDiff: CGFloat = CGFloat.greatestFiniteMagnitude
                
                let valuePerDivision = (maximiumValue - minimumValue) / CGFloat(divisions)
                for i in 0...divisions {
                    let divisionValue = minimumValue + CGFloat(i) * valuePerDivision
                    let diff = fabs(newValue - divisionValue)
                    if diff < closestDiff {
                        closestDiff = diff
                        closestIndex = i
                    }
                }
                
                newValue = minimumValue + CGFloat(closestIndex) * valuePerDivision
            } else {
                newValue = min(newValue, maximiumValue)
                newValue = max(newValue, minimumValue)
            }
            value = newValue
        }
        
        updateKnob()
    }
    
    private func updateKnob() {
        if showKnob {
            if knobView.superview == nil {
                let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                let knobView = TickKnobView(frame: frame)
                addSubview(knobView)
                self.knobView = knobView
            }
            bringSubview(toFront: knobView)
        } else {
            knobView.removeFromSuperview()
        }
        
        
        let normalizedValue = (value - minimumValue) / (maximiumValue - minimumValue)
        if orientation == .horizontal {
            let x: CGFloat = bounds.size.width * normalizedValue
            let y: CGFloat = bounds.midY
            let point = CGPoint(x: x, y: y)
            knobView.center = point
        } else {
            let x: CGFloat = bounds.midX
            let y: CGFloat = bounds.size.height - (bounds.size.height * normalizedValue)
            let point = CGPoint(x: x, y: y)
            knobView.center = point
        }
        
    }
    
    
}
