//
//  BlurView.swift
//  CustomSliderExample
//
//  Created by Zakk Hoyt on 6/7/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

// https://stackoverflow.com/questions/20531938/cigaussianblur-image-size

import UIKit


extension UIView {
    public func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let snapshotImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return snapshotImage
    }
}


class BlurView: UIView {

    var blurRadius: CGFloat = 16.0 {
        didSet {
//            update()
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .clear
    }
    
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.captureSuperview()
        }
    }
    

    private var imageView: UIImageView?
    
    private func captureSuperview() {
        
        guard let superview = self.superview else {
            assert(false, "no superview")
            return
        }
        
        let uiImage = superview.toImage()
        //let blurredImage = uiImage.applyBlur(withRadius: blurRadius, tintColor: nil /*UIColor.green.withAlphaComponent(0.2)*/, saturationDeltaFactor: 1.0, maskImage: nil)
        let blurredImage = uiImage.applyLightEffect()
        imageView?.removeFromSuperview()
        imageView = nil
        imageView = UIImageView(image: blurredImage)
        imageView?.alpha = 1.0
        imageView?.frame = self.bounds
        addSubview(imageView!)
    }
    
//    func blur(value: CGFloat) {
//        guard let superview = self.superview else {
//            assert(false, "no superview")
//            return
//        }
//        
//        let uiImage = superview.toImage()
//        let radius = blurInPixels * value
//        let blurredImage = uiImage.applyBlur(withRadius: radius, tintColor: nil /*UIColor.green.withAlphaComponent(0.2)*/, saturationDeltaFactor: 1.0, maskImage: nil)
//        
//        imageView?.removeFromSuperview()
//        imageView = nil
//        
//        imageView = UIImageView(image: blurredImage)
//        imageView?.frame = self.bounds
//        //imageView?.contentMode = .scaleAspectFill
//        addSubview(imageView!)
//    }

}
