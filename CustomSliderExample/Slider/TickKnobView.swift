//
//  TickKnobView.swift
//  CustomSliderExample
//
//  Created by Zakk Hoyt on 6/9/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import UIKit

class TickKnobView: UIView {

    var image: UIImage? {
        didSet {
            setNeedsDisplay()
        }
    }


    @IBInspectable public var color: UIColor = .white {
        didSet {
            setNeedsDisplay()
        }
    }
    

    @IBInspectable public var borderColor: UIColor = .lightGray {
        didSet {
            setNeedsDisplay()
        }
    }
    

    @IBInspectable public var borderWidth: CGFloat = 0.5 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = UIColor.clear
        
        layer.masksToBounds = false
        layer.shadowRadius = 1.0
        layer.shadowColor = UIColor.darkGray.withAlphaComponent(0.5).cgColor
        layer.shadowOpacity = 1.0
        layer.shadowOffset = CGSize(width: 0, height: 2)
    }
    
    public override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        let borderFrame = bounds.insetBy(dx: borderWidth / 2.0,  dy: borderWidth / 2.0)
        context.setFillColor(color.cgColor)
        context.addEllipse(in: borderFrame)
        context.fillPath()
        context.setLineWidth(borderWidth)
        context.setStrokeColor(borderColor.cgColor)
        context.addEllipse(in: borderFrame)
        context.strokePath()
        
        if let image = image {
            let percent: CGFloat = 0.5
            let width: CGFloat = bounds.width * percent
            let height: CGFloat = bounds.height * percent
            let x: CGFloat = (bounds.width - width) / 2.0
            let y: CGFloat = (bounds.height - height) / 2.0
            let frame = CGRect(x: x, y: y, width: width, height: height)
            //image.draw(in: frame)
            image.draw(in: frame, blendMode: CGBlendMode.exclusion, alpha: 1.0)
        }
        
    }
    
    
    
    
}
