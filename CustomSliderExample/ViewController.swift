//
//  ViewController.swift
//  CustomSliderExample
//
//  Created by Zakk Hoyt on 6/7/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//
//  UISlider
//  intrinsicHeight = 31
//
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var verticalSliderLabel: UILabel!
    
    @IBOutlet weak var verticalTickSlider: TickSlider!
    @IBOutlet weak var horizontalTickSlider: TickSlider!
    
    @IBOutlet weak var blurView: BlurView!
    
    var sliderBackgroundView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    
    fileprivate func setupSliderBackgroundView(for slider: UIControl) {
        if sliderBackgroundView == nil {
            sliderBackgroundView = UIView(frame: view.bounds)
            sliderBackgroundView?.isUserInteractionEnabled = false
            sliderBackgroundView?.backgroundColor = .orange
            view.addSubview(sliderBackgroundView!)
        }
        
        view.bringSubview(toFront: sliderBackgroundView!)
        view.bringSubview(toFront: slider)
        sliderBackgroundView?.alpha = 0
    }



    @IBAction func tickSliderValueChanged(_ sender: TickSlider) {
        print("tickSliderValueChanged: " + Date().description)
        verticalSliderLabel.text = String(format: "%.2f", sender.value)
    }
}

